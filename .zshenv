export XDG_CONFIG_HOME="$HOME"/.config
export XDG_DATA_HOME="$HOME"/.local/share
export XDG_CACHE_HOME="$HOME"/.cache
export ZDOTDIR="$HOME"/.config/zsh
export HISTFILE="$ZDOTDIR"/.zsh_history

## path
typeset -U PATH path
export PATH="/home/lamdacore/.bin:$PATH"
export PATH="/home/lamdacore/.emacs.d/bin:$PATH"
export PATH="/home/lamdacore/.local/share/cargo/bin:$PATH"
export SUDO_ASKPASS="/home/lamdacore/.bin/dmenupass"
export TERMINAL="st"
export BROWSER="firefox"
export READER="evince"
export LOCATION="Freiburg"
export VISUAL=mg
export EDITOR=mg
export PASSWORD_STORE_DIR="$HOME"/.password-store

## clean $HOME
export ANDROID_SDK_HOME="$XDG_CONFIG_HOME"/android
export ADB_VENDOR_KEY="$XDG_CONFIG_HOME"/android
export CARGO_HOME="$XDG_DATA_HOME"/cargo
export ELECTRUMDIR="$XDG_DATA_HOME/electrum"
export IPYTHONDIR="$XDG_CONFIG_HOME"/jupyter
export JUPYTER_CONFIG_DIR="$XDG_CONFIG_HOME"/jupyter
export RUSTUP_HOME="$XDG_DATA_HOME"/rustup
export WEECHAT_HOME="$XDG_CONFIG_HOME"/weechat
export LESSKEY="$XDG_CONFIG_HOME"/less/lesskey
export LESSHISTFILE="$XDG_CACHE_HOME"/less/history
export NOTMUCH_CONFIG="$XDG_CONFIG_HOME"/notmuch/notmuchrc
export NMBGIT="$XDG_DATA_HOME"/notmuch/nmbug
export OCTAVE_HISTFILE="$XDG_CACHE_HOME/octave-hsts"
export OCTAVE_SITE_INITFILE="$XDG_CONFIG_HOME/octave/octaverc"
export GOPATH="$XDG_DATA_HOME"/go
export ABDUCO_SOCKET_DIR="$XDG_CACHE_HOME"

## julia
export JULIA_CPU_THREADS=16
export JULIA_NUM_THREADS=16
export JULIA_EDITOR="emacs"

## firefox
export MOZ_X11_EGL=1
export MOZ_ACCELERATED=1
export MOZ_WEBRENDERER=1

## java
export _JAVA_OPTIONS='-Dawt.useSystemAAFontSettings=on -Dswing.aatext=true -Dswing.defaultlaf=com.sun.java.swing.plaf.gtk.GTKLookAndFeel -Dswing.crossplatformlaf=com.sun.java.swing.plaf.gtk.GTKLookAndFeel'

## fzf
# Use ~~ as the trigger sequence instead of the default **
# export FZF_COMPLETION_TRIGGER='~~'

# Options to fzf command
export FZF_COMPLETION_OPTS='--border --info=inline'

