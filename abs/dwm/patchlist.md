# Patchlist

DWM ([suckless.org](https://dwm.suckless.org/)) 6.2-1 with following patches:

1. [bottomstack](https://dwm.suckless.org/patches/bottomstack/dwm-bottomstack-6.1.diff)
2. [gaplessgrid](https://dwm.suckless.org/patches/gaplessgrid/dwm-gaplessgrid-6.1.diff)
3. [pertag](https://dwm.suckless.org/patches/pertag/dwm-pertag-6.2.diff)
4. [scratchpad](https://dwm.suckless.org/patches/scratchpad/dwm-scratchpad-6.2.diff)
5. [attachaside](https://dwm.suckless.org/patches/attachaside/dwm-attachaside-6.1.diff)
6. [uselessgaps](https://dwm.suckless.org/patches/uselessgap/dwm-uselessgap-6.2.diff)
7. systray - editted version of [dwm-systray-6.2](https://dwm.suckless.org/patches/systray/dwm-systray-6.2.diff)
8. swallow - editted version of [dwm-swallow-20200522-7accbcf](https://dwm.suckless.org/patches/swallow/dwm-swallow-20200522-7accbcf.diff)

Note: swallow doesn't seem to work right now.
